package ar.com.lalita.modelo;

import ar.com.lalita.interfac.Imprimible;

public class Producto implements Imprimible{
    protected String nombre;
    private Double precio;

    public Producto(String nombre, Double precio) {
        this.nombre = nombre;
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Override
    public void imprimir() {
        System.out.print("nombre= " + nombre  +
                " | precio= " + precio );
    }
}
