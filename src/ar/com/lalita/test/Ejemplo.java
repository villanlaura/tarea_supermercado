package ar.com.lalita.test;

import ar.com.lalita.generico.BolsaSupermercado;
import ar.com.lalita.modelo.*;

public class Ejemplo {
    public static void main(String[] args) {
        System.out.println(" Bolsa de Fruta: ");
        BolsaSupermercado<Producto> bolsaFruta = new BolsaSupermercado<>(5);
        bolsaFruta.addProducto(new Fruta("Naranja",3.2,1.2,"Naranja"));
        bolsaFruta.addProducto(new Fruta("Manzana",6.2,3.5,"Rojo"));
        bolsaFruta.addProducto(new Fruta("Pera",7.2,1.5,"Amarilla"));
        bolsaFruta.addProducto(new Fruta("Mandarina",5.2,2.8,"Naranja"));
        bolsaFruta.addProducto(new Fruta("Uva",10.2,1.5,"Blanco"));
        imprimirBolsa(bolsaFruta);

        System.out.println(" Bolsa de Lacteos: ");
        BolsaSupermercado<Producto> bolsaLacteo = new BolsaSupermercado<>(5);
        bolsaLacteo.addProducto(new Lacteo("Leche",5.3,2,200));
        bolsaLacteo.addProducto(new Lacteo("Yogurt",6.8,1,470));
        bolsaLacteo.addProducto(new Lacteo("Queso",123.6,3,970));
        bolsaLacteo.addProducto(new Lacteo("Manteca",60.8,2,570));
        bolsaLacteo.addProducto(new Lacteo("Ricota",33.2,1,870));
        imprimirBolsa(bolsaLacteo);

        System.out.println(" Bolsa de Limpieza: ");
        BolsaSupermercado<Producto> bolsaLimpieza = new BolsaSupermercado<>(5);
        bolsaLimpieza.addProducto(new Limpieza("lavandina",6.8,"comp1,comp2,comp3",1.5));
        bolsaLimpieza.addProducto(new Limpieza("Detergente",9.8,"comp2,comp3",1.0));
        bolsaLimpieza.addProducto(new Limpieza("Suavisante",4.4,"comp1,comp3",3.0));
        bolsaLimpieza.addProducto(new Limpieza("Jabon en Polvo",12.5,"comp2,comp7",1.0));
        bolsaLimpieza.addProducto(new Limpieza("Limpia Vidrios",9.8,"comp5,comp3",1.0));
        imprimirBolsa(bolsaLimpieza);

        System.out.println(" Bolsa de No Perecibles: ");
        BolsaSupermercado<Producto> bolsaNoPerecible = new BolsaSupermercado<>(5);
        bolsaNoPerecible.addProducto(new NoPerecible("Miel",9.3,100,700));
        bolsaNoPerecible.addProducto(new NoPerecible("Sal",5.2,1,754));
        bolsaNoPerecible.addProducto(new NoPerecible("Cafe",5.2,1,300));
        bolsaNoPerecible.addProducto(new NoPerecible("Azucar",5.2,1,170));
        bolsaNoPerecible.addProducto(new NoPerecible("Aceite",5.2,1,970));
        imprimirBolsa(bolsaNoPerecible);


        System.out.println(" Bolsa de cuando yo voy a comprar: ");
        BolsaSupermercado<Producto> bolsaSupermercado = new BolsaSupermercado<>(8);
        bolsaSupermercado.addProducto(new Fruta("Naranja",3.2,1.2,"Naranja"));
        bolsaSupermercado.addProducto(new Fruta("Manzana",6.2,3.5,"Rojo"));
        bolsaSupermercado.addProducto(new Lacteo("Leche",5.3,2,20));
        bolsaSupermercado.addProducto(new Lacteo("Yogurt",6.8,1,70));
        bolsaSupermercado.addProducto(new Limpieza("lavandina",6.8,"comp1,comp2,comp3",1.5));
        bolsaSupermercado.addProducto(new Limpieza("Detergente",9.8,"comp1,comp2,comp3",1.0));
        bolsaSupermercado.addProducto(new NoPerecible("Miel",9.3,100,700));
        bolsaSupermercado.addProducto(new NoPerecible("Sal",5.2,1,70));

        imprimirBolsa(bolsaSupermercado);
        System.out.println("******Otra Impresión *******");
        bolsaSupermercado.getProductos().stream().forEach(e -> e.imprimir());
        System.out.println("******Otra Impresión mas *******");
        bolsaSupermercado.getProductos().stream().forEach(Producto::imprimir);
    }


    public static <T extends Producto> void imprimirBolsa(BolsaSupermercado<T> bolsa) {
        for (T o : bolsa){
           o.imprimir();
        }
    }



}
