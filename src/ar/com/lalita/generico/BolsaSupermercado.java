package ar.com.lalita.generico;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BolsaSupermercado<T> implements Iterable<T> {
    private List<T> objetos;
    private int max;

    public BolsaSupermercado(int max) {
        this.max = max;
        this.objetos = new ArrayList<>();
    }

    public void addProducto(T objeto){
        if (this.objetos.size() <= max){
            this.objetos.add(objeto);
        } else {
            throw new RuntimeException("no mas espacio en lista");
        }
    }

    public List<T> getProductos(){
        return this.objetos;
    }


    @Override
    public Iterator<T> iterator() {
        return this.objetos.iterator();
    }


}
